// Fill out your copyright notice in the Description page of Project Settings.


#include "KeyHolderProxy.h"
#include "Funicular/Player/PlayerCharacterBase.h"

bool UKeyHolderProxy::CheckIfKeyHolderIsEmpty()
{
	return (StoredItems.Num() <= 0);
}

TArray<UItem*> UKeyHolderProxy::GetStoredItems()
{
	return StoredItems;
}

void UKeyHolderProxy::KeyPickedUp(TArray<int32> inKeyDoorIndexes, AActor* PickUpInsigator)
{
	APlayerCharacterBase* Player = Cast<APlayerCharacterBase>(PickUpInsigator);
	if (Player)
	{
		if (!Player->KeyHolderComponent)
			Player->CreateKeyHolderComponent(StoredItems);

		Player->KeyHolderComponent->KeyPickedUp(inKeyDoorIndexes);
	}
}
