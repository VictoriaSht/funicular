// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Funicular/Keys/KeyHolderComponent.h"
#include "KeyHolderProxy.generated.h"

/**
 * 
 */
UCLASS()
class FUNICULAR_API UKeyHolderProxy : public UObject
{
	GENERATED_BODY()
protected:
	// ItemsInfo
	TArray<UItem*> StoredItems;

public:
	bool CheckIfKeyHolderIsEmpty();

	TArray<UItem*> GetStoredItems();

	void KeyPickedUp(TArray<int32> inKeyDoorIndexes, AActor* PickUpInsigator);
};
