// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Funicular/Interaction/InteractableActorBase.h"
#include "Funicular/Interaction/Interfaces/InterfaceInteractE.h"
#include "KeyActor.generated.h"

/**
 * 
 */
UCLASS()
class FUNICULAR_API AKeyActor : public AInteractableActorBase, public IInterfaceInteractE
{
	GENERATED_BODY()
public:
	// Sets default values for this actor's properties
	AKeyActor();

	// Doors this key can open
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "KeyStats")
	TArray<int32> KeyDoorIndexes;



	/* Show tip */

	// Show tip component
	class UShowTipComponent* ShowTipComponent{ nullptr };



	/* Inherited from base */

	// Player overlaped this actor
	virtual void PlayerOverlap() override;



	/* Interactions from interfaces */

	// Interact function from InterfaceInteractE
	virtual void InteractE(AActor* InteractInstigator) override;
};
