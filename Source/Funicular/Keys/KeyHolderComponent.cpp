// Fill out your copyright notice in the Description page of Project Settings.


#include "KeyHolderComponent.h"
#include "Funicular/FunicularRepairManager.h"

// UStoredKey
UStoredKey* UStoredKey::Clone()
{
	UStoredKey* Copy = new UStoredKey;
	if (Copy)
	{
		Copy->KeyDoorIndexes = KeyDoorIndexes;
	}
	return Copy;
}

///////////////////////////////

// Sets default values for this component's properties
UKeyHolderComponent::UKeyHolderComponent()
{
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}

// Key picked up by owner from world
void UKeyHolderComponent::KeyPickedUp(TArray<int32> inKeyDoorIndexes)
{
	UStoredKey* NewKey = new UStoredKey;
	StoredItems.Add(NewKey);
	UE_LOG(LogTemp, Verbose, TEXT("UKeyHolderComponent::AddKey - New key added."));

	OnAddItem.Broadcast(EItemType::Key);
}

// Create clone of the key
UStoredKey* UKeyHolderComponent::DuplicateKey(UStoredKey* OriginalKey)
{
	UStoredKey* NewKey{ nullptr };
	NewKey = OriginalKey->Clone();
	return NewKey;
}

// Called from KeyHolder widget
void UKeyHolderComponent::CloneKey(int32 KeyIndex)
{
	if (UFunicularRepairManager::Get()->TakeParts(ERobotLoot::MechanicalParts, 2))
	{
		StoredItems.Add(DuplicateKey(static_cast<UStoredKey*>(StoredItems[KeyIndex])));
		OnAddItem.Broadcast(EItemType::Key);
	}
}

void UKeyHolderComponent::AddKeyToBunch_Implementation(int32 KeyIndex)
{
	UItem* RemovedKey = StoredItems[KeyIndex];
	StoredItems.Remove(RemovedKey);
	OnDestroyItem.Broadcast(KeyIndex);

	UKeyBunch* KeyBunch = nullptr;
	for (UItem* elem : StoredItems)
	{
		KeyBunch = static_cast<UKeyBunch*>(elem);
		if (KeyBunch)
			break;
	}

	if (!KeyBunch)
	{
		KeyBunch = new UKeyBunch;
		StoredItems.Add(KeyBunch);
		OnAddItem.Broadcast(EItemType::KeyBunch);
	}


	KeyBunch->KeysInBunch.Add(static_cast<UStoredKey*>(RemovedKey));
	delete RemovedKey;

}

// Called from KeyHolder widget
void UKeyHolderComponent::DropItem_Implementation(int32 KeyIndex)
{
	// in BP
}
