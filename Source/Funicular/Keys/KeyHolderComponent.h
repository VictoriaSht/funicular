// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "KeyHolderComponent.generated.h"

UENUM(BlueprintType)
enum class EItemType : uint8
{
	Key UMETA(DiaplayName = "Key"),
	KeyBunch UMETA(DisplayName = "KeyBunch")
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAddItem, EItemType, ItemType);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnDestroyItem, int32, ItemIndex);

struct UItem
{
	//
};

struct UStoredKey : public UItem
{
	TArray<int32> KeyDoorIndexes;

	// Copy this key
	UStoredKey* Clone();
};

struct UKeyBunch : public UItem
{
	TArray<UStoredKey*> KeysInBunch;
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class FUNICULAR_API UKeyHolderComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UKeyHolderComponent();

	// Delegate for widget
	UPROPERTY(BlueprintAssignable, Category = "KeyHolderWidget")
	FOnAddItem OnAddItem;

	// Delegate for widget
	UPROPERTY(BlueprintAssignable, Category = "KeyHolderWidget")
	FOnDestroyItem OnDestroyItem;

	// ItemsInfo
	TArray<UItem*> StoredItems;

	// Add key to Holder
	void KeyPickedUp(TArray<int32> inKeyDoorIndexes);

	// Create key duplicate
	UStoredKey* DuplicateKey(UStoredKey* OriginalKey);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "KeyHolder")
	void DropItem(int32 KeyIndex);

	UFUNCTION(BlueprintCallable, Category = "KeyHolder")
	void CloneKey(int32 KeyIndex);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "KeyHolder")
	void AddKeyToBunch(int32 KeyIndex);
	void AddKeyToBunch_Implementation(int32 KeyIndex);
};
