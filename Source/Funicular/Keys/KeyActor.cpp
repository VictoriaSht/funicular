// Fill out your copyright notice in the Description page of Project Settings.


#include "KeyActor.h"
#include "KeyHolderComponent.h"
#include "Funicular/Interaction/ShowTipComponent.h"
#include "Funicular/Player/PlayerCharacterBase.h"
#define LOCTEXT_NAMESPACE "TipTexts"

AKeyActor::AKeyActor()
{
	// Create and set 'show tip component'
	ShowTipComponent = CreateDefaultSubobject<UShowTipComponent>(TEXT("ShowTipComp"));
	ShowTipComponent->myTipText = LOCTEXT("Press 'E' to pick up the key", "Нажми 'E', чтобы поднять ключ");
}

// Player overlapped
void AKeyActor::PlayerOverlap()
{
	// Show tip widget
	ShowTipComponent->ShowTip();
}

// Interact "E" triggered
void AKeyActor::InteractE(AActor* InteractInstigator)
{
	// Add this key to Instigator's KeyHolder
	//UKeyHolderComponent* KeyHolder = Cast<UKeyHolderComponent>(InteractInstigator->GetComponentByClass(UKeyHolderComponent::StaticClass()));
	APlayerCharacterBase* Player = Cast<APlayerCharacterBase>(InteractInstigator);
	if (Player)
		Player->KeyHolderProxy->KeyPickedUp(KeyDoorIndexes, InteractInstigator);

	// Destroy actor
	Destroy();
}
