// Fill out your copyright notice in the Description page of Project Settings.


#include "ActivatorPanelActor.h"
#include "Funicular/Interaction/ShowTipComponent.h"
#include "Funicular/EnemyActivation/ActivatorComponent/EnemyActivatorComponent.h"
#define LOCTEXT_NAMESPACE "TipTexts"

AActivatorPanelActor::AActivatorPanelActor()
{
	// Create and set 'show tip component'
	ShowTipComponent = CreateDefaultSubobject<UShowTipComponent>(TEXT("ShowTipComp"));
	ShowTipComponent->myTipText = LOCTEXT("Press 'E' to activate enemies and become able to shoot them", "Нажми 'E', чтобы активировать роботов и разблокировать стрельбу по врагам");

	// Create and set 'activator component'
	ActivatorComponent = CreateDefaultSubobject<UEnemyActivatorComponent>(TEXT("ActivatorComp"));
}

void AActivatorPanelActor::PlayerOverlap()
{
	ShowTipComponent->ShowTip();
}

void AActivatorPanelActor::InteractE(AActor* InteractInstigator)
{
	if (!bWasActivatedOnce)
	{
		bWasActivatedOnce = true;
		ActivatorComponent->ActivateEnemies();
		ShowTipComponent->myTipText = LOCTEXT("Press 'E' to respawn enemies in this room", "Нажми 'E', чтобы возродить врагов в комнате");
	}
}
