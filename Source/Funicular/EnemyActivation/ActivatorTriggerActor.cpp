// Fill out your copyright notice in the Description page of Project Settings.


#include "ActivatorTriggerActor.h"

AActivatorTriggerActor::AActivatorTriggerActor()
{
	// Create and set 'activator component'
	ActivatorComponent = CreateDefaultSubobject<UEnemyActivatorComponent>(TEXT("ActivatorComp"));
}

void AActivatorTriggerActor::PlayerOverlap()
{
	ActivatorComponent->ActivateEnemies();
}
