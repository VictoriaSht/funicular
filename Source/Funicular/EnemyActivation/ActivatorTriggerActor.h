// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Funicular/Interaction/InteractableActorBase.h"
#include "Funicular/EnemyActivation/ActivatorComponent/EnemyActivatorComponent.h"
#include "ActivatorTriggerActor.generated.h"

/**
 * 
 */
UCLASS()
class FUNICULAR_API AActivatorTriggerActor : public AInteractableActorBase
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AActivatorTriggerActor();

	// Activator component
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Components)
	class UEnemyActivatorComponent* ActivatorComponent{ nullptr };

	// Player overlaped this actor (from InteractableActorBase)
	virtual void PlayerOverlap() override;
};
