// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Funicular/Interaction/InteractableActorBase.h"
#include "Funicular/Interaction/Interfaces/InterfaceInteractE.h"
#include "ActivatorPanelActor.generated.h"

/**
 * 
 */
UCLASS()
class FUNICULAR_API AActivatorPanelActor : public AInteractableActorBase, public IInterfaceInteractE
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	AActivatorPanelActor();

	// Activator component
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Components)
	class UEnemyActivatorComponent* ActivatorComponent{ nullptr };

	// Show tip component
	class UShowTipComponent* ShowTipComponent{ nullptr };

	// Player overlaped this actor (from InteractableActorBase)
	virtual void PlayerOverlap() override;

	// Interact function from InterfaceInteractR
	virtual void InteractE(AActor* InteractInstigator) override;

	// For changind logic to respawning enemies after first activation
	bool bWasActivatedOnce{ false };
};
