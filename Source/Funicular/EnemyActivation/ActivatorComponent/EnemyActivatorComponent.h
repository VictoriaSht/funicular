// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "EnemyActivatorComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class FUNICULAR_API UEnemyActivatorComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UEnemyActivatorComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	// Tagged actors will be remembered on enemies activation for childen implementations
	TArray<AActor*> TaggedActors;

public:	
	// Enemies activation
	virtual void ActivateEnemies();

	// All enemies with this tag will activate
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "EnemyActivation")
	FName MyTag;
};
