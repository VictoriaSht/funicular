// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyActivatorComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Funicular/Enemies/EnemyBase.h"

// Sets default values for this component's properties
UEnemyActivatorComponent::UEnemyActivatorComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UEnemyActivatorComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

void UEnemyActivatorComponent::ActivateEnemies()
{
	if (!MyTag.IsNone())
	{
		UGameplayStatics::GetAllActorsWithTag(GetWorld(), MyTag, TaggedActors);
		for (auto elem : TaggedActors)
		{
			AEnemyBase* Enemy = Cast<AEnemyBase>(elem);
			if (Enemy)
				Enemy->ActivateEnemy();
		}
	}
}



