// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Funicular/Interaction/InteractableActorBase.h"
#include "Funicular/Interaction/Interfaces/InterfaceInteractE.h"
#include "Funicular/Interaction/Interfaces/InterfaceInteractR.h"
#include "CoolerActor.generated.h"

/**
 * 
 */
UCLASS()
class FUNICULAR_API ACoolerActor : public AInteractableActorBase, public IInterfaceInteractE, public IInterfaceInteractR
{
	GENERATED_BODY()
public:
	// Sets default values for this actor's properties
	ACoolerActor();

	// Water amount this cooler have
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CoolerStats")
	int32 WaterReserve = 100;


	/* Show tip */

	// Show tip component
	class UShowTipComponent* ShowTipComponent{ nullptr };



	/* Inherited from base */

	// Player overlaped this actor
	virtual void PlayerOverlap() override;



	/* Interactions from interfaces */

	// Interact function from InterfaceInteractE
	virtual void InteractE(AActor* InteractInstigator) override;

	// Interact function from InterfaceInteractR
	virtual void InteractR(AActor* InteractInstigator) override;
};
