// ill out your copyright notice in the Description page of Project Settings.


#include "CoolerActor.h"
#include "Funicular/Interaction/ShowTipComponent.h"
#include "Funicular/Player/PlayerCharacterBase.h"
#define LOCTEXT_NAMESPACE "TipTexts"

ACoolerActor::ACoolerActor()
{
	// Create and set 'show tip component'
	ShowTipComponent = CreateDefaultSubobject<UShowTipComponent>(TEXT("ShowTipComp"));
	ShowTipComponent->myTipText = LOCTEXT("Press 'E' to heal, press 'R' to set spawn point", "Нажми 'E', чтобы восполнить здоровье, 'R' - чтобы установить точку воскрешения");
}

// Player overlapped
void ACoolerActor::PlayerOverlap()
{
	// Show tip widget
	ShowTipComponent->ShowTip();
}

// Interact "E" triggered
void ACoolerActor::InteractE(AActor* InteractInstigator)
{
	// Get player
	APlayerCharacterBase* Player = Cast<APlayerCharacterBase>(InteractInstigator);
	if (Player)
	{
		// Spent water and heal player
		int32 WaterSpent = 25;
		WaterReserve -= WaterSpent;
		if (WaterReserve < 0)
		{
			// If reserve goes below zero
			WaterReserve = 0;
			WaterSpent += WaterReserve;
		}
		Player->Heal(WaterSpent);
	}
} 

// Interact "R" triggered
void ACoolerActor::InteractR(AActor* InteractInstigator)
{
	// If cooler have water, player can set spawn point
	if (WaterReserve > 0)
	{
		// Set spawn point
		APlayerCharacterBase* Player = Cast<APlayerCharacterBase>(InteractInstigator);
		if (Player)
			Player->SetSpawnPoint(this);
	}
}
