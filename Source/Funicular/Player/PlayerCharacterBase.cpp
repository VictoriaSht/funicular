// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCharacterBase.h"
#include "Funicular/Interaction/Interfaces/InterfaceInteractE.h"
#include "Funicular/Interaction/Interfaces/InterfaceInteractR.h"
#include "Funicular/Interaction/InteractionAdapter.h"

// Sets default values
APlayerCharacterBase::APlayerCharacterBase()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//CreateDefaultSubobject<UKeyHolderComponent>(TEXT("KeyHolderComp"));
}

// Called when the game starts or when spawned
void APlayerCharacterBase::BeginPlay()
{
	Super::BeginPlay();

	//KeyHolderProxy = NewObject<UKeyHolderProxy>();
}

// Interact "E" input
void APlayerCharacterBase::InteractE()
{
	TArray<AActor*> OverlappedActors;
	GetOverlappingActors(OverlappedActors);
	for (auto elem : OverlappedActors)
	{
		IInterfaceInteractE* EInteractInterface = Cast<IInterfaceInteractE>(elem);
		if (EInteractInterface)
			EInteractInterface->InteractE(this);
		else
		{
			UInteractionAdapter* IntAdapter = Cast<UInteractionAdapter>(elem->GetComponentByClass(UInteractionAdapter::StaticClass()));
			if (IntAdapter)
				IntAdapter->InteractE(this);
		}
	}
}

// Interact "R" input
void APlayerCharacterBase::InteractR()
{
	TArray<AActor*> OverlappedActors;
	GetOverlappingActors(OverlappedActors);
	for (auto elem : OverlappedActors)
	{
		IInterfaceInteractR* RInteractInterface = Cast<IInterfaceInteractR>(elem);
		if (RInteractInterface)
			RInteractInterface->InteractR(this);
	}
}

// Called every frame
void APlayerCharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerCharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Binding input actions
	if (InputComponent)
	{
		InputComponent->BindAction("InteractE", IE_Pressed, this, &APlayerCharacterBase::InteractE);
		InputComponent->BindAction("InteractR", IE_Pressed, this, &APlayerCharacterBase::InteractR);
	}
}

void APlayerCharacterBase::SetSpawnPoint(AActor* inSpawnCooler)
{
	CurrentSpawnPoint = inSpawnCooler;
}

void APlayerCharacterBase::CreateKeyHolderComponent(TArray<UItem*> inStoredItems)
{
	// Create KeyHolder Component
	//KeyHolderComponent = NewObject<UKeyHolderComponent>(this, TEXT("KeyHolderComp"));
	//if (KeyHolderComponent)
	//{
	//	KeyHolderComponent->StoredItems = inStoredItems;
	//	CreateKeyHolderWidget(KeyHolderComponent);
	//}
}

void APlayerCharacterBase::CreateKeyHolderWidget_Implementation(UKeyHolderComponent* KeyHolder)
{
	//in BP
}

void APlayerCharacterBase::Heal_Implementation(int32 HealAmount)
{
}

