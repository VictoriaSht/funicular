// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "PlayerControllerBase.generated.h"

/**
 * 
 */
UCLASS()
class FUNICULAR_API APlayerControllerBase : public APlayerController
{
	GENERATED_BODY()
	
public:
	// Show interact tip widget
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "PlayerWidgets")
	void ShowTipWidget(const FText& TipText);

};
