// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Funicular/Keys/KeyHolderComponent.h"
#include "Funicular/Keys/KeyHolderProxy.h"
#include "PlayerCharacterBase.generated.h"

UCLASS()
class FUNICULAR_API APlayerCharacterBase : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerCharacterBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Interact by "E" key
	void InteractE();
	// Interact by "R" key
	void InteractR();

	// Current spawn point actor
	AActor* CurrentSpawnPoint{ nullptr };

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// KeyHolder component
	UPROPERTY(BlueprintReadWrite, Category = Components)
	UKeyHolderComponent* KeyHolderComponent{ nullptr };

	// HeyHolder Proxy
	UKeyHolderProxy* KeyHolderProxy;

	// Heal
	UFUNCTION(BlueprintNativeEvent, Category = "Health")
	void Heal(int32 HealAmount);

	// Set spawn point
	void SetSpawnPoint(AActor* inSpawnCooler);

	UFUNCTION(BlueprintNativeEvent, Category = "KeyHolderWidget")
	void CreateKeyHolderWidget(UKeyHolderComponent* KeyHolder);

	void CreateKeyHolderComponent(TArray<UItem*> inStoredItems);
};
