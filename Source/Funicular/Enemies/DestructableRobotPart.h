// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Funicular/FunicularRepairManager.h"
#include "DestructableRobotPart.generated.h"

UCLASS()
class FUNICULAR_API ADestructableRobotPart : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADestructableRobotPart();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Part's loot
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RobotDestructablePart")
	ERobotLoot LootType;

	// Is this part destructed
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RobotDestructablePart")
	bool bDestructed = false;

};
