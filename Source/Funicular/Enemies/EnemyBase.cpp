// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyBase.h"
#include "Funicular/RobotLoot/RobotLootBuilder.h"
#include "Funicular/Interaction/InteractionAdapter.h"
#include "DestructableRobotPart.h"
#define LOCTEXT_NAMESPACE "TipTexts"

// Sets default values
AEnemyBase::AEnemyBase()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AEnemyBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AEnemyBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AEnemyBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AEnemyBase::GenerateLoot()
{
	// Create builder
	auto LootBuilder = NewObject<URobotLootBuilder>(this);

	// GetAll child components
	TArray<AActor*> ChildActors;
	GetAllChildActors(ChildActors, false);

	for (auto elem : ChildActors)
	{
		ADestructableRobotPart* Part = Cast<ADestructableRobotPart>(elem);
		// If part is not destructed, generate specific loot
		if (Part && !Part->bDestructed)
		{
			switch (Part->LootType)
			{
			case ERobotLoot::MechanicalParts:
				LootBuilder->GenerateMechanicalParts();
				break;
			case ERobotLoot::EnergyBlocks:
				LootBuilder->GenerateEnergyBlocks();
				break;
			case ERobotLoot::Chips:
				LootBuilder->GenerateChips();
				break;
			case ERobotLoot::Ammunition:
				LootBuilder->GenerateAmmunition();
				break;
			case ERobotLoot::PhotoFilters:
				LootBuilder->GeneratePhotoFilters();
				break;
			}
		}
	}

	LootBuilder->ConditionalBeginDestroy();
}

void AEnemyBase::InteractE()
{
	InstantKillEnemy();
}

void AEnemyBase::InstantKillEnemy_Implementation()
{
}

void AEnemyBase::ActivateEnemy_Implementation()
{
	UInteractionAdapter* InteractionAdapter = NewObject<UInteractionAdapter>(this, TEXT("InteractionAdapterComp"));
	InteractionAdapter->InitShowTipObject(LOCTEXT("'E' to kill", "убить - 'E'"));
	InteractionAdapter->OnInteractionE.AddDynamic(this, &AEnemyBase::InteractE);
}

