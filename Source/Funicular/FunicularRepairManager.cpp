// Fill out your copyright notice in the Description page of Project Settings.


#include "FunicularRepairManager.h"

UFunicularRepairManager::UFunicularRepairManager()
{
}

void UFunicularRepairManager::AddParts(ERobotLoot PartsType, int32 PartsCount)
{
	if (CollectedParts.Contains(PartsType))
	{
		int32 CurrentCount = *CollectedParts.Find(PartsType);
		CollectedParts.Add(PartsType, CurrentCount + PartsCount);
	}
	else
	{
		CollectedParts.Add(PartsType, PartsCount);
	}
}

bool UFunicularRepairManager::TakeParts(ERobotLoot PartsType, int32 PartsCount)
{
	bool result = false;

	if (CollectedParts.Contains(PartsType))
	{
		int32 CurrentCount = *CollectedParts.Find(PartsType);
		if (CurrentCount >= PartsCount)
		{
			CollectedParts.Add(PartsType, CurrentCount - PartsCount);
			result = true;
		}
	}
	
	return result;
}
