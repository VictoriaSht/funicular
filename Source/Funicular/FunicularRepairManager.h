// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "FunicularRepairManager.generated.h"

UENUM(BlueprintType)
enum class ERobotLoot : uint8
{
	MechanicalParts UMETA(DisplayName = "Mecanical Parts"),
	Ammunition UMETA(DisplayName = "Ammunition"),
	EnergyBlocks UMETA(DisplayName = "Energy Blocks"),
	PhotoFilters UMETA(DisplayName = "Photo Filters"),
	Chips UMETA(DisplayName = "Chips")
};

// Manager's instance
static UFunicularRepairManager* Instance;

/**
 * 
 */
UCLASS()
class FUNICULAR_API UFunicularRepairManager : public UObject
{
	GENERATED_BODY()

private:
	UFunicularRepairManager();

public:
	// Returns instance. If there is no instance, create and return it
	static UFunicularRepairManager* Get();

	// Parts needed for funicular
	TMap<ERobotLoot, int32> NeededParts;

	// Parts players have at the moment
	TMap<ERobotLoot, int32> CollectedParts;

	// Player collected robot loot
	void AddParts(ERobotLoot PartsType, int32 PartsCount);

	// Take parts from collected parts
	bool TakeParts(ERobotLoot PartsType, int32 PartsCount);

};

inline UFunicularRepairManager* UFunicularRepairManager::Get()
{
	if (!Instance)
	{
		Instance = NewObject<UFunicularRepairManager>();
	}
	return Instance;
}

