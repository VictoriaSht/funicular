// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ShowTipComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class FUNICULAR_API UShowTipComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UShowTipComponent();

	// TipText
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "TipText")
	FText myTipText;

	// Create tip widget from player conrtoller
	void ShowTip();
};
