// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "InterfaceInteractE.generated.h"

UINTERFACE(MinimalAPI)
class UInterfaceInteractE : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class FUNICULAR_API IInterfaceInteractE
{
	GENERATED_BODY()

public:
	// Player interacts with object by "E" input
	virtual void InteractE(AActor* InteractInstigator)	PURE_VIRTUAL(UInterfaceInteractE::InteractE,);
};
