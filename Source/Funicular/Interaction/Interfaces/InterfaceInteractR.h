// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "InterfaceInteractR.generated.h"

UINTERFACE(MinimalAPI)
class UInterfaceInteractR : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class FUNICULAR_API IInterfaceInteractR
{
	GENERATED_BODY()

public:
	// Player interacts with object by "R" input
	virtual void InteractR(AActor* InteractInstigator)	PURE_VIRTUAL(UInterfaceInteractR::InteractR,);
};
