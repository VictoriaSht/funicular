// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Funicular/Interaction/Interfaces/InterfaceInteractE.h"
#include "InteractionAdapter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPlayerOverlap);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnInteractionE);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class FUNICULAR_API UInteractionAdapter : public UActorComponent, public IInterfaceInteractE
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UInteractionAdapter();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	// For overlap with player 
	UFUNCTION()
	void ActorBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

public:	
	// Player Overlapped
	FOnPlayerOverlap OnPlayerOverlap;


	/* SHOW TIP*/

	// For initiating shoing tip
	void InitShowTipObject(FText inTipText);

	// Show tip component
	class UShowTipComponent* ShowTipComponent{ nullptr };


	/* INTERACTION "E"*/

	// Delegate for binding
	FOnInteractionE OnInteractionE;

	// Interact function from InterfaceInteractE
	virtual void InteractE(AActor* InteractInstigator) override;
};
