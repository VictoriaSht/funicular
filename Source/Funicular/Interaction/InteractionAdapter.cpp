// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractionAdapter.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Funicular/Interaction/ShowTipComponent.h"

// Sets default values for this component's properties
UInteractionAdapter::UInteractionAdapter()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UInteractionAdapter::BeginPlay()
{
	Super::BeginPlay();

	// ...
	USphereComponent* SphereCollider = Cast<USphereComponent>(GetOwner()->GetComponentByClass(USphereComponent::StaticClass()));
	if (SphereCollider)
		SphereCollider->OnComponentBeginOverlap.AddDynamic(this, &UInteractionAdapter::ActorBeginOverlap);
	
}

void UInteractionAdapter::ActorBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AActor* Player = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	if (OtherActor == Player)
		OnPlayerOverlap.Broadcast();

	if (ShowTipComponent)
		ShowTipComponent->ShowTip();
}

// Init show tip object
void UInteractionAdapter::InitShowTipObject(FText inTipText)
{
	// Create and set 'show tip component'
	ShowTipComponent = NewObject<UShowTipComponent>(GetOwner(), TEXT("ShowTipComp"));
	ShowTipComponent->myTipText = inTipText;
}

void UInteractionAdapter::InteractE(AActor* InteractInstigator)
{
	OnInteractionE.Broadcast();
}

