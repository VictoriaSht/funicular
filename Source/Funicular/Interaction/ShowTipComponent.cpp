// Fill out your copyright notice in the Description page of Project Settings.


#include "ShowTipComponent.h"
#include "Funicular/Player/PlayerControllerBase.h"
#include "Kismet/GameplayStatics.h"

// Sets default values for this component's properties
UShowTipComponent::UShowTipComponent()
{
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}

void UShowTipComponent::ShowTip()
{
	// Create tip widget from player conrtoller
	APlayerControllerBase* PC = Cast<APlayerControllerBase>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	if (PC)
		PC->ShowTipWidget(myTipText);
}

