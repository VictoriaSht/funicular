// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InteractableActorBase.generated.h"

UCLASS()
class FUNICULAR_API AInteractableActorBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AInteractableActorBase();

	// Virtual destructor because this is base class for children
	virtual ~AInteractableActorBase()
	{}

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Player interacts with this actor by overlapping
	UFUNCTION(BlueprintCallable)
	virtual void PlayerOverlap()	PURE_VIRTUAL(AInteractableActorBase::Overlap,);

	// Overlap sphere
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Components)
	class USphereComponent* OverlapSphere{ nullptr };

	// Sphere begin overlap bind function
	UFUNCTION()
	void CollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
};
