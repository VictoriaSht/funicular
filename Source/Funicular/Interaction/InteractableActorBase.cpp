// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractableActorBase.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AInteractableActorBase::AInteractableActorBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Creating default scene root
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));

	// Creating overlap sphere
	OverlapSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Overlap Sphere"));
	OverlapSphere->OnComponentBeginOverlap.AddDynamic(this, &AInteractableActorBase::CollisionSphereBeginOverlap);
	OverlapSphere->SetupAttachment(RootComponent);
	OverlapSphere->SetHiddenInGame(false);
}

// Called when the game starts or when spawned
void AInteractableActorBase::BeginPlay()
{
	Super::BeginPlay();
}

void AInteractableActorBase::CollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AActor* Player = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	if (OtherActor == Player)
		PlayerOverlap();
}
