// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Engine/DataTable.h"
#include "Funicular/FunicularRepairManager.h"
#include "RobotLootBuilder.generated.h"

USTRUCT(BlueprintType)
struct FRobotLootInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RobotLootStats")
	ERobotLoot LootType = ERobotLoot::Ammunition;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RobotLootStats")
	TArray<UStaticMesh*> LootStaticMeshes;
};

/**
 * 
 */
UCLASS()
class FUNICULAR_API URobotLootBuilder : public UObject
{
	GENERATED_BODY()
	
public:
	URobotLootBuilder();

	void GenerateMechanicalParts();
	void GenerateAmmunition();
	void GenerateEnergyBlocks();
	void GeneratePhotoFilters();
	void GenerateChips();

	// Spawn loot actor at outer actor's transform
	class ARobotLoot* SpawnLootActor();

	// Data table with loot info
	UDataTable* PartsInfoDataTable;

	// Get random mesh from data table info
	UStaticMesh* GetRandomMesh(ERobotLoot LootType);
};
