// Fill out your copyright notice in the Description page of Project Settings.


#include "RobotLoot.h"
#include "Components/StaticMeshComponent.h"

ARobotLoot::ARobotLoot()
{
	// Creating mesh component
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(RootComponent);
	Mesh->SetCollisionResponseToChannels(ECollisionResponse::ECR_Ignore);
}

// Player overlaped actor
void ARobotLoot::PlayerOverlap()
{
	UFunicularRepairManager::Get()->AddParts(LootType, LootCount);
	Destroy();
}


// Initiate loot
void ARobotLoot::InitLoot(ERobotLoot inLootType, int32 inLootCount, UStaticMesh* inMesh)
{
	LootType = inLootType;
	LootCount = inLootCount;
	Mesh->SetStaticMesh(inMesh);
}
