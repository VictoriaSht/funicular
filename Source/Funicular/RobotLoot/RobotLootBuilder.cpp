// Fill out your copyright notice in the Description page of Project Settings.


#include "RobotLootBuilder.h"
#include "RobotLoot.h"
#include "Kismet/KismetMathLibrary.h"

URobotLootBuilder::URobotLootBuilder()
{
	ConstructorHelpers::FObjectFinder<UDataTable> LootInfoTableAsset(TEXT("/Game/Funicular/Blueprints/FromNewCode/DT_RobotLootInfo.DT_RobotLootInfo"));
	if (LootInfoTableAsset.Succeeded())
		PartsInfoDataTable = LootInfoTableAsset.Object;
}

void URobotLootBuilder::GenerateMechanicalParts()
{
	ARobotLoot* Loot = SpawnLootActor();
	UStaticMesh* Mesh = GetRandomMesh(ERobotLoot::MechanicalParts);
	if (Loot && Mesh)
		Loot->InitLoot(ERobotLoot::MechanicalParts, 1, Mesh);
	
}

void URobotLootBuilder::GenerateAmmunition()
{
	ARobotLoot* Loot = SpawnLootActor();
	UStaticMesh* Mesh = GetRandomMesh(ERobotLoot::Ammunition);
	if (Loot && Mesh)
		Loot->InitLoot(ERobotLoot::Ammunition, 1, Mesh);
}

void URobotLootBuilder::GenerateEnergyBlocks()
{
	ARobotLoot* Loot = SpawnLootActor();
	UStaticMesh* Mesh = GetRandomMesh(ERobotLoot::EnergyBlocks);
	if (Loot && Mesh)
		Loot->InitLoot(ERobotLoot::EnergyBlocks, 1, Mesh);
}

void URobotLootBuilder::GeneratePhotoFilters()
{
	ARobotLoot* Loot = SpawnLootActor();
	UStaticMesh* Mesh = GetRandomMesh(ERobotLoot::PhotoFilters);
	if (Loot && Mesh)
		Loot->InitLoot(ERobotLoot::PhotoFilters, 1, Mesh);
}

void URobotLootBuilder::GenerateChips()
{
	ARobotLoot* Loot = SpawnLootActor();
	UStaticMesh* Mesh = GetRandomMesh(ERobotLoot::PhotoFilters);
	if (Loot && Mesh)
		Loot->InitLoot(ERobotLoot::Chips, 1, Mesh);
}

ARobotLoot* URobotLootBuilder::SpawnLootActor()
{
	if (GetOuter())
	{
		AActor* OuterActor = Cast<AActor>(GetOuter());
		if (OuterActor)
		{
			FVector SpawnLocation = UKismetMathLibrary::RandomPointInBoundingBox(OuterActor->GetActorLocation(), FVector(300, 300, 0));
			FRotator SpawnRotator = OuterActor->GetActorRotation();
			FActorSpawnParameters SpawnParams;
			SpawnParams.Owner = OuterActor;
			SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
			ARobotLoot* SpawnedLoot = OuterActor->GetWorld()->SpawnActor<ARobotLoot>(SpawnLocation, SpawnRotator, SpawnParams);
			return SpawnedLoot;
		}
	}

	return nullptr;
}

UStaticMesh* URobotLootBuilder::GetRandomMesh(ERobotLoot LootType)
{
	UStaticMesh* Result = nullptr;

	TArray<FRobotLootInfo*> Rows;
	PartsInfoDataTable->GetAllRows<FRobotLootInfo>("", Rows);
	for (auto elem : Rows)
	{
		if (elem->LootType == LootType)
		{
			int32 RandInt = FMath::RandHelper(elem->LootStaticMeshes.Num() - 1);
			Result = elem->LootStaticMeshes[RandInt];
		}
	}

	return Result;
}
