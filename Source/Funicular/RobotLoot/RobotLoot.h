// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Funicular/Interaction/InteractableActorBase.h"
#include "Funicular/FunicularRepairManager.h"
#include "RobotLoot.generated.h"

/**
 * 
 */
UCLASS()
class FUNICULAR_API ARobotLoot : public AInteractableActorBase
{
	GENERATED_BODY()
public:
	ARobotLoot();

	/* Inherited from base */

	// Player overlaped this actor
	virtual void PlayerOverlap() override;

private:
	/* Defaults */

	// Type of this loot
	ERobotLoot LootType;
	// How much loot it will add
	int32 LootCount = 0;

public:
	// Initiate this loot actor
	void InitLoot(ERobotLoot inLootType, int32 inLootCount, UStaticMesh* inMesh);

	// Static mesh
	class UStaticMeshComponent* Mesh = nullptr;
};
